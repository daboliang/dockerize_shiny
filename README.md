# Make sure you have docker installed

https://docs.docker.com/install/

# Build the image

```
docker build -t dockerized-shiny .
```

# Run the container

```
docker run --rm -p 3838:3838 dockerized-shiny
```